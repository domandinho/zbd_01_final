import time
import mysql.connector
import psycopg2
from configuration import *
import sys

class FirstTaskConfiguration:
    NUMBER_OF_BYTES_PER_READ = 8 * 1024


class AbstractTaskSolver:

    def _current_time(self):
        millis = int(round(time.time() * 1000))
        return millis


class FirstTaskSolver(AbstractTaskSolver):
    def read_data_from_file(self, number_of_bytes, path_to_file):
        file = open(path_to_file, "rb")
        start_time = self._current_time()
        try:
            self._read_data(file, number_of_bytes)
        finally:
            file.close()
        end_time = self._current_time()
        self._print_data(start_time, end_time)

    def _read_data(self, file, number_of_bytes):
        byte = file.read(number_of_bytes)
        i = 1
        while byte != "":
            if (i % 1000 == 1):
                self._print_progres()
            i += 1
            byte = file.read(number_of_bytes)
        print 'Done'

    def _print_progres(self):
        sys.stdout.write('X')

    def _print_data(self, start_time, end_time):
        print "Measurement ended"
        total_time = end_time - start_time
        print "Reading tooks ", (total_time / 1000).__str__(), ".", (total_time % 1000).__str__(), " seconds."

class SecondTaskSolver(AbstractTaskSolver):
    def create_sql_insert_script(self):
        for i in range(0, 1000):
            print "insert into all_users(login, password) values(", i.__str__(), ",", (i + 1).__str__(), ");"

    def count_delay(self, mysql_configuration):
        mysql_measurement = MySQLSecondTaskResolver()
        mysql_measurement.count_delay(mysql_configuration)
        postgresql_measurement = PostgresqlSecondTaskResolver()
        postgresql_measurement.count_delay()

    def compare_two_mysql_databases(self, first_mysql_configuration, second_mysql_configuration, query):
        self.__count_delay_mysql(first_mysql_configuration, query)
        self.__count_delay_mysql(second_mysql_configuration, query)

    def count_delay_on_concrete_query(self, mysql_configuration, mysql_query, postgresql_query):
        self.__count_delay_mysql(mysql_connect_configuration, mysql_query)
        self.__count_delay_postgresql(postgresql_query)

    def __count_delay_mysql(self, mysql_configuration, mysql_query):
        mysql_measurement = MySQLSecondTaskResolver()
        mysql_measurement.sql_query = mysql_query
        mysql_measurement.count_delay(mysql_configuration)

    def __count_delay_postgresql(self, postgresql_query):
        postgresql_measurement = PostgresqlSecondTaskResolver()
        postgresql_measurement.sql_query = postgresql_query
        postgresql_measurement.count_delay()

    def __get_seconds(self, value):
        return (value / 1000).__str__()

    def __get_miliseconds(self, value):
        result = (value % 1000).__str__()
        if (value < 100):
            result = '0' + result
        if (value < 10):
            result = '0' + result
        return "." + result

    def _print_results(self, start_time, connect_time, end_time):
        first_time = connect_time - start_time
        second_time = end_time - connect_time
        print 'Connecting tooks ', self.__get_seconds(first_time) + self.__get_miliseconds(first_time)
        print 'Executing tooks ', self.__get_seconds(second_time) + self.__get_miliseconds(second_time)
        first_time += 0.0
        second_time += 0.0
        if (first_time < 0.001):
            first_time = 0.001
        print 'Relation = ', (second_time / first_time)

class MySQLSecondTaskResolver(SecondTaskSolver):

    sql_query = "SELECT * FROM perf"

    def _execute_query(self, mysql_connection):
        cursor = mysql_connection.cursor()
        query = (self.sql_query)
        cursor.execute(query)
        x = 0
        for (value) in cursor:
            x = value

    def count_delay(self, configuration):
        print "MYSQL test:"
        start_time = self._current_time()
        try:
            mysql_connection = mysql.connector.connect(**configuration)
            connect_time = self._current_time()
            self._execute_query(mysql_connection)
            end_time = self._current_time()
            self._print_results(start_time, connect_time, end_time)
        finally:
            mysql_connection.close()


class PostgresqlSecondTaskResolver(SecondTaskSolver):

    sql_query = """SELECT * from perf"""

    def count_delay(self):
        print "PostgreSQL test"
        start_time = self._current_time()
        try:
            connection = psycopg2.connect(POSTGRESQL_CONFIGURATION)
            connect_time = self._current_time()
            self._execute_query(connection)
            end_time = self._current_time()
            self._print_results(start_time, connect_time, end_time)
        except:
            print "I am unable to connect to the database"

    def _execute_query(self, connection):
        cursor = connection.cursor()
        cursor.execute(self.sql_query)
        for (value) in cursor:
            x = value


class FileSystemCompare:

    def task_1():
        print 'Reading by 8 kilobytes blocks'
        FileSystemCompare.__read_data_session()
        FirstTaskConfiguration.NUMBER_OF_BYTES_PER_READ = 32 * 1024
        print 'Reading by 32 kilobytes blocks'
        FileSystemCompare.__read_data_session()
        print 'Reading by 128 kilobytes blocks'
        FirstTaskConfiguration.NUMBER_OF_BYTES_PER_READ = 128 * 1024
        FileSystemCompare.__read_data_session()
    task_1 = staticmethod(task_1)

    def __read_data_session():
        file_counter = FirstTaskSolver()
        print FirstTaskConfiguration.NUMBER_OF_BYTES_PER_READ
        print 'Hard drive '
        file_counter.read_data_from_file(FirstTaskConfiguration.NUMBER_OF_BYTES_PER_READ, "/home/domandinho/TEST_ASSET_1.5GB_")
        print 'Hard drive cache'
        file_counter.read_data_from_file(FirstTaskConfiguration.NUMBER_OF_BYTES_PER_READ, "/home/domandinho/TEST_ASSET_1.5GB_")
        print 'Pendrive'
        file_counter.read_data_from_file(FirstTaskConfiguration.NUMBER_OF_BYTES_PER_READ, "/media/7F0D-3F76/TEST_ASSET_1.5GB")
        print 'DVD-ROM'
        file_counter.read_data_from_file(FirstTaskConfiguration.NUMBER_OF_BYTES_PER_READ, "/media/cdrom/TEST_ASSET_1.5GB")
    __read_data_session = staticmethod(__read_data_session)

class LocalPostgresAndMysqlCompare:
    def run_query(query):
        database_connector = SecondTaskSolver()
        database_connector.count_delay_on_concrete_query(mysql_connect_configuration, query, query)
    print
    run_query= staticmethod(run_query)

    def task_2():
        print
        print '[3 000 000 RECORDS] SELECT *:'
        database_connector = SecondTaskSolver()
        database_connector.count_delay(mysql_connect_configuration)
        print

        print '[3 000 000 RECORDS] SELECT * where id = 666. About 3 results'
        LocalPostgresAndMysqlCompare.run_query("SELECT * FROM perf WHERE id = 666")

        print '[3 000 000 RECORDS] SELECT * WHERE id %=. About 30000 results'
        LocalPostgresAndMysqlCompare.run_query("SELECT * FROM perf WHERE id % 100000 = 3333")

        print '[1000 records] SELECT * FROM CROSS JOIN with equal conditions'
        LocalPostgresAndMysqlCompare.run_query("SELECT * FROM all_users a, all_users b where a.login = b.password and a.password = 13")

        print '[1000 records] SELECT * FROM CROSS JOIN with math conditions'
        LocalPostgresAndMysqlCompare.run_query("SELECT * FROM all_users a, all_users b where a.login * b.password = a.password")

        print '[1000 records] Select from 3 crosses with equal condition. About 2000 results'
        LocalPostgresAndMysqlCompare.run_query("SELECT * FROM all_users a, all_users b, all_users c where a.login = 13 and c.login = 13")

        print '[3 000 000 RECORDS] Select count(*) from join'
        LocalPostgresAndMysqlCompare.run_query("SELECT count(*) FROM all_users a, all_users b, all_users c where a.login = 13 or b.password = 55 and c.login = a.login")
    task_2 = staticmethod(task_2)

class LocalAndOnlineComparator(AbstractTaskSolver):

    def run_mysql_query(query):
        print
        second_task_resolver = MySQLSecondTaskResolver()
        second_task_resolver.compare_two_mysql_databases(mysql_connect_configuration, mysql_connect_configuration_online, query)
        print
    run_mysql_query = staticmethod(run_mysql_query)

    def local_vs_online():
        LocalAndOnlineComparator.run_mysql_query("select * from all_users")
        LocalAndOnlineComparator.run_mysql_query("select count(*) from all_users where login = 35")
        LocalAndOnlineComparator.run_mysql_query("select * from all_users a1, all_users a2 where a1.login = a2.password")
        LocalAndOnlineComparator.run_mysql_query("select * from all_users a1, all_users a2, all_users a3 where a1.login = a2.password and a2.login = a3.password")
    local_vs_online = staticmethod(local_vs_online)
